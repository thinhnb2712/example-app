<?php

namespace App\Http\Requests;

use http\Env\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'body' => 'required'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = new \Illuminate\Http\Response([
            'errors' => $validator->errors(),
        ], 422);
        throw (new ValidationException($validator,$response));
    }

    public function messages()
    {
        return [
            'name.required' => 'Không được để trống :attribute',
            'body.required' => 'Không được để trống :attribute',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Họ và tên',
            'body' => 'nội dung'
        ];
    }
}
